
# Palette Widget

Qt widget for viewing color tables and picking colors. It can be used in both Qt4 and Qt5 projects.

There is no library project for this widget. It's supposed that you just use source files in your own project and change them as you wish.

You may generate html documentation with doxygen:

    doxygen Doxyfile

Also see example in 'example' directory.

See UNLICENSE for license information.
