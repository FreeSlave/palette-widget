#include "palettewidget.h"

#include <QKeyEvent>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPainter>
#include <QtGlobal>
#include <cmath>
#include <QDebug>

PaletteWidget::PaletteWidget(QWidget *parent)
    : QWidget(parent)
{
    construct();
}

PaletteWidget::PaletteWidget(const QVector<QRgb> &colorTable, QWidget *parent)
    : QWidget(parent), _colorTable(colorTable)
{
    construct();
}

void PaletteWidget::construct()
{
    _columnNumber = 0;
    _cellSize = QSize(16, 16);
    _chosen = -1;
    _fixedCellSize = true;
    _selectionPen.setWidth(2);
    _selectionPen.setStyle(Qt::DashLine);

    setContentsMargins(4,4,4,4);
}

PaletteWidget::~PaletteWidget()
{

}

QSize PaletteWidget::sizeHint() const
{
    QSize toReturn;

    QSize cSize = cellSize();
    QMargins margins = contentsMargins();
    int colNum = !isFixedColumnNumber() ? integerSqrt(colorCount()) : columnNumber();
    int rowNum = qRound(ceil(static_cast<float>(colorCount())/colNum));

    toReturn.setWidth(colNum*cSize.width() + margins.left() + margins.right());
    toReturn.setHeight(rowNum*cSize.height() + margins.top() + margins.bottom());

    return toReturn;
}

void PaletteWidget::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);
    QPainter painter(this);
    painter.setPen(_framePen);
    int cNumber = columnNumber();

    int x = cellsX();
    int y = cellsY();
    QSize cSize = cellSize();
    for (int i=0; i<_colorTable.size(); ++i)
    {
        if (i % cNumber == 0 && i != 0)
        {
            y += cSize.height();
            x = cellsX();
        }

        painter.setBrush(QBrush(QColor(_colorTable[i])));
        painter.drawRect(x, y, cSize.width(), cSize.height());
        x += cSize.width();
    }

    QPoint pos = posFromIndex(_chosen);
    if (pos.x() >= 0 && pos.y() >=0)
    {
        QPen pen(_selectionPen);

        QColor cellColor = color(_chosen);
        QColor opposite = oppositeColor(cellColor);
        pen.setColor(opposite);
        painter.setPen(pen);
        painter.setBrush(Qt::NoBrush);

        painter.drawRect(pos.x(), pos.y(), cSize.width(), cSize.height());
    }
}

void PaletteWidget::mousePressEvent(QMouseEvent *event)
{
    int index = indexFromPos(event->pos());
    setCurrent(index);
}

void PaletteWidget::keyPressEvent(QKeyEvent *event)
{
    int colNum = columnNumber();
    int n = currentIndex() / colNum;
    int m = currentIndex() % colNum;

    int rowNum = qRound(ceil(colorCount()/static_cast<float>(colNum)));

    switch (event->key())
    {
    case Qt::Key_Left:
        if (currentIndex() < 0)
            setCurrent(0);
        else if (m>0)
            setCurrent(n*colNum + m-1);
        else if (n>0)
            setCurrent((n-1)*colNum + colNum-1);
        break;
    case Qt::Key_Right:
        if (currentIndex() < 0)
            setCurrent(0);
        else if (m<colNum-1)
            setCurrent(n*colNum + m+1);
        else if (n<rowNum-1)
            setCurrent((n+1)*colNum);
        break;
    case Qt::Key_Up:
        if (currentIndex() < 0)
            setCurrent(0);
        else if (n>0)
            setCurrent((n-1)*colNum + m);
        break;
    case Qt::Key_Down:
        if (currentIndex() < 0)
            setCurrent(0);
        else if (n<rowNum-1)
            setCurrent((n+1)*colNum + m);
        break;
    default:
        break;
    }

    QWidget::keyPressEvent(event);
}

int PaletteWidget::columnNumber() const
{
    if (!isFixedColumnNumber())
    {
        if (isFixedCellSize())
        {
            float aspectRatio = static_cast<float>(cellsHeight())/static_cast<float>(cellsWidth());
            float f = ceil(colorCount()/aspectRatio);
            int toReturn = qRound(sqrt(f));
            return toReturn ? toReturn : 1;
        }
        else
        {
            int toReturn = integerSqrt(colorCount());
            return toReturn ? toReturn : 1;
        }
    }
    else
        return _columnNumber;
}

void PaletteWidget::setColumnNumber(int columnNumber)
{
    if (_columnNumber != columnNumber)
    {
        _columnNumber = columnNumber;
        update();
    }
}

void PaletteWidget::setColor(int index, QRgb rgb)
{
    if (index >= 0 && index < colorCount() && color(index) != rgb)
    {
        _colorTable[index] = rgb;
        update();
    }
}

void PaletteWidget::setColorTable(const QVector<QRgb> &colorTable)
{
    _colorTable = colorTable;
    resetSelection();
}

void PaletteWidget::resizeColorTable(int i)
{
    if (i != _colorTable.size())
    {
        _colorTable.resize(i);
        resetSelection();
    }
}

void PaletteWidget::resetSelection()
{
    _chosen = -1;
    update();
}

QSize PaletteWidget::cellSize() const
{
    if (isFixedCellSize())
    {
        return _cellSize;
    }
    else
    {
        int colNum = columnNumber();
        int rowNum = qRound(ceil(static_cast<float>(colorCount())/colNum));
        return QSize(cellsWidth()/colNum, cellsHeight()/rowNum);
    }
}

void PaletteWidget::setCellSize(const QSize &size)
{
    if (_cellSize != size)
    {
        _cellSize = size;
        if (isFixedCellSize())
            update();
    }
}

int PaletteWidget::cellsX() const
{
    return contentsMargins().left();
}

int PaletteWidget::cellsY() const
{
    return contentsMargins().top();
}

int PaletteWidget::cellsWidth() const
{
    int toReturn = width() - contentsMargins().left() - contentsMargins().right();
    if (toReturn < 1)
        return 1;
    return toReturn;
}

int PaletteWidget::cellsHeight() const
{
    int toReturn = height() - contentsMargins().top() - contentsMargins().bottom();
    if (toReturn < 1)
        return 1;
    return toReturn;
}

int PaletteWidget::indexFromPos(const QPoint &pos) const
{
    QRect cellsRect(cellsX(), cellsY(), cellsWidth(), cellsHeight());

    if (cellsRect.contains(pos))
    {
        QSize cSize = cellSize();
        int x = pos.x() - cellsRect.left();
        int m = x / cSize.width();
        int y = pos.y() - cellsRect.top();
        int n = y / cSize.height();

        int index = n*columnNumber() + m;

        if (index < colorCount())
        {
            return index;
        }
    }
    return -1;
}

QPoint PaletteWidget::posFromIndex(int index) const
{
    if (index >=0 && index < colorCount())
    {
        int colNum = columnNumber();
        int n = index / colNum;
        int m = index % colNum;

        QSize cSize = cellSize();
        return QPoint(cSize.width()*m + cellsX(), cSize.height()*n + cellsY());
    }
    return QPoint(-1, -1);
}

void PaletteWidget::setCurrent(int index)
{
    if (index < colorCount() && currentIndex() != index)
    {
        _chosen = index;
        emit colorChosen(currentIndex(), currentColor());
        update();
    }
}

void PaletteWidget::setFixedCellSize(bool fixed)
{
    if (_fixedCellSize != fixed)
    {
        _fixedCellSize = fixed;
        update();
    }
}

void PaletteWidget::setSelectionPen(const QPen& pen)
{
    if (_selectionPen != pen)
    {
        _selectionPen = pen;
        if (_chosen >= 0)
            update();
    }
}

void PaletteWidget::setFramePen(const QPen &framePen)
{
    if (_framePen != framePen)
    {
        _framePen = framePen;
        update();
    }
}

QColor PaletteWidget::oppositeColor(const QColor &color) const
{
    QColor toReturn;
    if (color.value() > 0x7F)
        toReturn.setHsv(color.hue(), color.saturation(), 0x0);
    else
        toReturn.setHsv(color.hue(), color.saturation(), 0xFF);
    return toReturn;
}

int PaletteWidget::integerSqrt(int i)
{
    return qRound(ceil(sqrt(i)));
}
