#include "palettedialog.h"

#include <QFormLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpinBox>

PaletteDialog::PaletteDialog(QWidget *parent) :
    QDialog(parent)
{
    _currentIndexLabel = new QLabel;
    _colorEdit = new QLineEdit;
    _checkFixed = new QCheckBox;
    _checkFixed->setChecked(true);

    QSpinBox* columnSpinBox = new QSpinBox;
    columnSpinBox->setMinimum(0);
    columnSpinBox->setSpecialValueText("Auto");

    QFormLayout* formLayout = new QFormLayout;
    formLayout->addRow(tr("Index: "), _currentIndexLabel);
    formLayout->addRow(tr("Color: "), _colorEdit);
    formLayout->addRow(tr("Columns: "), columnSpinBox);
    formLayout->addRow(tr("Fixed cell size"), _checkFixed);

    QVector<QRgb> pal(256);
    for (int i=0; i<pal.size(); ++i)
        pal[i] = qRgb(0x7F,i,0xFF-i);

    _palWidget = new PaletteWidget;
    _palWidget->setColorTable(pal);
    _palWidget->setFocusPolicy(Qt::StrongFocus);
    _palWidget->setMinimumSize(_palWidget->sizeHint());

    QHBoxLayout* hbox = new QHBoxLayout;
    hbox->addWidget(_palWidget,1);
    hbox->addLayout(formLayout,1);
    setLayout(hbox);

    connect(_palWidget, SIGNAL(colorChosen(int,QRgb)), SLOT(colorSlot(int,QRgb)));
    connect(_checkFixed, SIGNAL(toggled(bool)), _palWidget, SLOT(setFixedCellSize(bool)));
    connect(_colorEdit, SIGNAL(editingFinished()), SLOT(changeCurrentColor()));
    connect(columnSpinBox, SIGNAL(valueChanged(int)), _palWidget, SLOT(setColumnNumber(int)));
}

void PaletteDialog::colorSlot(int index, QRgb rgb)
{
    if (index >= 0)
    {
        _currentIndexLabel->setText(QString::number(index));
        _colorEdit->setText(colorToText(rgb));
    }
    else
    {
        _currentIndexLabel->clear();
        _colorEdit->clear();
    }
}

void PaletteDialog::changeCurrentColor()
{
    QColor color = colorFromText(_colorEdit->text());
    if (color.isValid())
        _palWidget->setColor(_palWidget->currentIndex(), color.rgba());
}

QString PaletteDialog::colorToText(const QRgb &rgb)
{
    return QString("%1 %2 %3 %4").arg(qRed(rgb)).arg(qGreen(rgb)).arg(qBlue(rgb)).arg(qAlpha(rgb));
}

QColor PaletteDialog::colorFromText(const QString &text)
{
    QStringList list = text.split(' ', QString::SkipEmptyParts);
    QColor toReturn;
    if (list.size() >= 3 && list.size() <= 4)
    {
        bool ok;
        int red = list.at(0).toInt(&ok);
        if (!ok || red < 0 || red > 255)
            return toReturn;
        int green = list.at(1).toInt(&ok);
        if (!ok || green < 0 || green > 255)
            return toReturn;
        int blue = list.at(2).toInt(&ok);
        if (!ok || blue < 0 || blue > 255)
            return toReturn;

        if (list.size() == 4)
        {
            int alpha = list.at(3).toInt(&ok);
            if (!ok || alpha < 0 || alpha > 255)
                return toReturn;
            toReturn.setAlpha(alpha);
        }

        toReturn.setRed(red);
        toReturn.setGreen(green);
        toReturn.setBlue(blue);

        return toReturn;
    }
    return toReturn;
}
