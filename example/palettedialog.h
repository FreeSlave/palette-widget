#ifndef PALETTEDIALOG_H
#define PALETTEDIALOG_H

#include <QColor>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>

#include "palettewidget.h"

class PaletteDialog : public QDialog
{
    Q_OBJECT
public:
    explicit PaletteDialog(QWidget *parent = 0);

signals:

public slots:
    void colorSlot(int index, QRgb rgb);
    void changeCurrentColor();

private:
    static QColor colorFromText(const QString& text);
    static QString colorToText(const QRgb& rgb);

    QLabel* _currentIndexLabel;
    QLineEdit* _colorEdit;
    QCheckBox* _checkFixed;

    PaletteWidget* _palWidget;
};

#endif // PALETTEDIALOG_H
