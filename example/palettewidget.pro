#-------------------------------------------------
#
# Project created by QtCreator 2014-08-26T18:08:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = palettewidgetexample
TEMPLATE = app


SOURCES += main.cpp\
        ../palettewidget.cpp \
    palettedialog.cpp

HEADERS  += ../palettewidget.h \
    palettedialog.h

INCLUDEPATH += ..
